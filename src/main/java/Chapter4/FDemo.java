package Chapter4;

/*
Демонстрация метода finalize().
 */
public class FDemo {
    int x;

    FDemo(int i) {
        x = 1;
    }

    // Вызывается при удалении объекта.
    protected void finalize() {
        System.out.println("Финализация " + x);
    }

    // Генерирует объект, который тотчас уничтожается
    void generator(int i) {
        FDemo o = new FDemo(i);
    }
}

class Finalize {
    public static void main(String args[]) {
        int count;

        FDemo ob = new FDemo(0);

        // Генерируем много объектов.

        for (count = 1; count < 1000000000; count++) {
            ob.generator(count);
        }
    }
}