package Chapter4;

/*
Демонстрация работы метода.
 */
public class AddMeth {
    public static void main(String args[]) {
        Vehicle minivan = new Vehicle();
        Vehicle sportscar = new Vehicle();

        int range1, range2;

        // Присвоение значений полям minivan
        minivan.passengers = 17;
        minivan.fuelcap = 50;
        minivan.mpg = 10;

        // Присвоение значений полям sportscar
        sportscar.passengers = 2;
        sportscar.fuelcap = 20;
        sportscar.mpg = 15;

        System.out.println("Мини-фургон может перевезти " + minivan.passengers + " пассажиров.");
        minivan.range(); // Отображение дальности поездки минивена.

        System.out.println("Спорткар может перевезти " + sportscar.passengers + " пассажиров.");
        sportscar.range(); // Отображение дальности поездки спорткара.
    }
}
