package Chapter4;

/*
Демонстрация конструктора.
 */
class MyParmClass {
    int x;

    MyParmClass(int i) { // Параметризованный конструктор класса. Мы передаем значение i при создании экземпляра.
        x = i;
    }
}

public class ParmConsClass {
    public static void main(String args[]) {
        MyParmClass t1 = new MyParmClass(10); // Передача значений при объявлении.
        MyParmClass t2 = new MyParmClass(50);

        System.out.println(t1.x + " " + t2.x);
    }
}
