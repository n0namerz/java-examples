package Chapter4;

/*
Более 1го параметра в методе.
 */
public class Factor {
    boolean isFactor(int a, int b) { // Метод имеет 2 параметра, a и b
        if ((b % a) == 0) return true;
        else return false;
    }
}

class IsFact {
    public static void main(String args[]) {
        Factor x = new Factor();

        if (x.isFactor(2, 20)) System.out.println("2 - делитель.");
        if (x.isFactor(3, 20)) System.out.println("Строка не будет выведена, т.к. условие метода не выполняется.");
    }
}