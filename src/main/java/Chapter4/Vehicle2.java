package Chapter4;

/*
Параметризированные конструкторы
 */
class Vehicle2 {
    int passengers;
    int fuelcap;
    int mpg;

    Vehicle2(int p, int f, int m) {
        passengers = p;
        fuelcap = f;
        mpg = m;
    }

    // Вернуть целое число содержащее дальность поездки транспортного средства.
    int range() {
        return fuelcap * mpg;
    }

    // Метод возвращающий количество необходимого топлива для преодоленя переданного аргументом расстояния.
    double fuelneeded(int miles) {
        return (double) miles / mpg;
    }
}

class VehConsDemo {
    public static void main(String args[]) {
        Vehicle2 minivan = new Vehicle2(15, 50, 12);
        Vehicle2 sportscar = new Vehicle2(2, 25, 15);
        double gallons;
        int miles = 250;

        gallons = minivan.fuelneeded(miles);
        System.out.println("Для преодоления " + miles + " миль, минивену понадобится " + gallons + " галлонов топлива.");

        gallons = sportscar.fuelneeded(miles);
        System.out.println("Для преодоления " + miles + " миль, спорткару понадобится " + gallons + " галлонов топлива.");
    }
}