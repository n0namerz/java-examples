package Chapter4;

/*
Использование методов, которые возвращают значения.
 */
public class RetMeth {
    public static void main(String args[]) {
        Vehicle minivan = new Vehicle();
        Vehicle sportscar = new Vehicle();

        int minivanRange, sportscarRange;

        minivan.passengers = 17;
        minivan.fuelcap = 50;
        minivan.mpg = 10;

        sportscar.passengers = 2;
        sportscar.fuelcap = 15;
        sportscar.mpg = 10;

        minivanRange = minivan.range(); // Присвоение переменной значение путем вызова метода, который возвращает число.
        sportscarRange = sportscar.range();

        System.out.println("Мини-фургон может перевезти " + minivan.passengers + " пассажиров. Дальность - " + minivanRange + " миль.");
        System.out.println("Спорткар может перевезти " + sportscar.passengers + " пассажиров. Дальность - " + sportscarRange + " миль.");
    }
}
