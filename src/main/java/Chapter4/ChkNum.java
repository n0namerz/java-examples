package Chapter4;

/*
Применение параметров в методе.
 */
class ChkNum {
    boolean isEven(int x) { // Целочисленный параметр метода isEven()
        if ((x % 2) == 0) return true;
        else return false;
    }
}

class ParmDemo {
    public static void main(String args[]) {
        ChkNum i = new ChkNum();

        if (i.isEven(10)) System.out.println("10 - четное число"); // 10 - это аргумент передаваемый методу
        if (i.isEven(9)) System.out.println("9 - четное число");
        if (i.isEven(8)) System.out.println("8 - четное число");
    }
}