package Chapter4;

/*
Первый вариант класса.
 */
class Vehicle {
    int passengers; // Количество пассажиров
    int fuelcap; // Емкость топливного бака
    int mpg; // Потребление топлива в милях на галлон

    // Вернуть целое число содержащее дальность поездки транспортного средства.
    int range() {
        return fuelcap * mpg;
    }

    // Метод возвращающий количество необходимого топлива для преодоленя переданного аргументом расстояния.
    double fuelneeded(int miles) {
        return (double) miles / mpg;
    }
}

class VehicleDemo {
    public static void main(String args[]) {
        Vehicle minivan = new Vehicle();
        int range;

        // Присвоение значений полям в объекте minivan
        minivan.passengers = 7;
        minivan.fuelcap = 16;
        minivan.mpg = 21;

        // Расчитать дальность поездки на полном баке
        range = minivan.fuelcap * minivan.mpg;
        System.out.println("Мини-фургон может перевезти " + minivan.passengers + " пассажиров на расстояние " + range + " миль.");
    }
}

class TwoVehicles {
    public static void main(String args[]) {
        Vehicle minivan = new Vehicle();
        Vehicle sportscar = new Vehicle();

        int range1, range2;

        // Присвоить значение полям экземпляра minivan.
        minivan.passengers = 7;
        minivan.fuelcap = 16;
        minivan.mpg = 21;

        // Присвоить значение полям экземпляра sportscar.
        sportscar.passengers = 2;
        sportscar.fuelcap = 10;
        sportscar.mpg = 15;

        // Рассчет расстояния для каждого экземпляра.
        range1 = minivan.fuelcap * minivan.mpg;
        range2 = sportscar.fuelcap * sportscar.mpg;

        System.out.println("Мини-фургон может перевезти " + minivan.passengers + " пассажиров на " + range1 + " миль.");
        System.out.println("Спорткар может перевезти " + sportscar.passengers + " пассажиров на " + range2 + " миль.");
    }
}

class CompFuel {
    public static void main(String args[]) {
        Vehicle minivan = new Vehicle();
        Vehicle sportscar = new Vehicle();
        double gallons;
        int miles = 250;

        // Присвоить значение полям экземпляра minivan.
        minivan.passengers = 7;
        minivan.fuelcap = 16;
        minivan.mpg = 21;

        // Присвоить значение полям экземпляра sportscar.
        sportscar.passengers = 2;
        sportscar.fuelcap = 10;
        sportscar.mpg = 15;

        gallons = minivan.fuelneeded(miles);
        System.out.println("Для преодоления " + miles + " миль, минивену понадобится " + gallons + " галлонов топлива.");

        gallons = sportscar.fuelneeded(miles);
        System.out.println("Для преодоления " + miles + " миль, спорткару понадобится " + gallons + " галлонов топлива.");
    }
}