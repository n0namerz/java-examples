package Chapter5;

/*
Демонстрация использования массивов.
 */
public class ArrayDemo {
    public static void main(String args[]) {
        int array[] = new int[10]; // Объявление массива содержащего целочисленные значения в количестве 10 штук

        for (int i = 0; i < 10; i++) {
            array[i] = i;
        }

        for (int i = 0; i < 10; i++) {
            System.out.println("Содержимое массива по индексу " + i + " равен: " + array[i]);
        }
    }
}
