package Chapter5;

/*
Перебор массива. Нахождение минимального и максимального числа.
 */
public class MinMax2 {
    public static void main(String args[]) {
        int array[] = {-1, -2, -3, 1, 6, 0, 5, 4, 3, 2}; // Более быстрый и красивый способ заполнить массив.
        int min, max;
        int i;

        min = max = array[0];

        for (i = 1; i < 10; i++) {
            if (array[i] < min) min = array[i];
            if (array[i] > max) max = array[i];
        }
        System.out.println("min = " + min + ", max = " + max);
    }
}
