package Chapter5;

/*
Перебор массива. Нахождение минимального и максимального числа.
 */
public class MinMax {
    public static void main(String args[]) {
        int array[] = new int[10];
        int min, max;
        int i;

        // Долгий, но рабочий вариант заполнения массива.
        array[0] = -1;
        array[1] = -2;
        array[2] = -3;
        array[3] = 1;
        array[4] = 6;
        array[5] = 0;
        array[6] = 5;
        array[7] = 4;
        array[8] = 3;
        array[9] = 1;
        min = max = array[0];


        for (i = 1; i < 10; i++) {
            if (array[i] < min) min = array[i];
            if (array[i] > max) max = array[i];
        }
        System.out.println("min = " + min + ", max = " + max);
    }
}
