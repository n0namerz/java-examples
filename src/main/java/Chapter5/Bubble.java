package Chapter5;

/*
Демонстрация пузырьковой сортировки.
 */
public class Bubble {
    public static void main(String args[]) {
        int array[] = {5, 6, 7, 8, 1, 9, 2, 4, 3, 0};
        int a, b, c;

        // Отобразить исходный массив.
        System.out.print("Исходный массив: ");
        for (a = 0; a < 10; a++) {
            System.out.print(" " + array[a]);
        }
        System.out.println();

        // Сортировка
        for (a = 0; a < 10; a++) { // Этот цикл нужен, чтобы прогнать внутренний цикл для каждого элемента массива.
            for (b = 9; b > a; b--) { // В этом идет сравнение соседних элементов.
                if (array[b - 1] > array[b]) { // Если предыдущий элемент меншье следующего, то они меняются местами.
                    c = array[b - 1];
                    array[b - 1] = array[b];
                    array[b] = c;
                }
            }
        }
        // Отобразить отсортированный массив.
        System.out.print("Отсортированный массив: ");
        for (a = 0; a < 10; a++) {
            System.out.print(" " + array[a]);
        }
        System.out.println();
    }
}
