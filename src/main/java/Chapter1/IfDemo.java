package Chapter1;

/*
Демонстрация использования оператора if.
 */
public class IfDemo {
    public static void main(String args[]) {
        int a, b, c;
        a = 2;
        b = 3;

        if (a < b) System.out.println("a меньше чем b.");
        // Следующая строка никогда не будет выведена
        if (a == b) System.out.println("Вы не должны увидеть этот текст.");

        System.out.println();

        c = a - b; // c содержит значение -1
        System.out.println("c содержит -1");

        if (c > 0) System.out.println("с - положительное число.");
        if (c < 0) System.out.println("c - отрицательное число.");

        System.out.println();

        c = b - a; // c содержит значение 1
        System.out.println("с содержит 1");
        if (c > 0) System.out.println("с - положительное число.");
        if (c < 0) System.out.println("с - отрицательное число.");


    }
}
