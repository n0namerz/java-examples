package Chapter1;

/*
Демонстрация использования блоков кода.
 */
public class BlockDemo {
    public static void main(String args[]) {
        double i, j, d;

        i = 5;
        j = 10;

        // Телом оператора будет целый блок
        if (i != 0) {
            System.out.println("i не ровняется нулю.");
            d = i / j;
            System.out.println("i/j равно " + d);

        }
    }
}
