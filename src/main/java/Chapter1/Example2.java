package Chapter1;

/*
Демонстрация использования переменных.
 */
public class Example2 {
    public static void main(String args[]) {
        int var1; // объявляется переменная
        int var2; // объявляется вторая переменная
        int var3, var4; // можно объявлять переменные в одном операторе объявления

        var1 = 1024; // переменной присваивается значение 1024
        System.out.println("Значение переменной var1 = " + var1);

        var2 = var1 / 2; // второй переменной присваивается значение
        System.out.print("Переменная var2 содержит значение var1/2: ");
        System.out.println(var2);
    }
}
