package Chapter1;

/*
Первое управжнение.
Переводим галлоны в литры.
 */
public class GalToLit {
    public static void main(String args[]) {
        double gallons; // объявляем переменную, которая будет хранить галлоны
        double liters; // объявляем переменную, которая будет хранить литры

        gallons = 10; // изначальное количество в галлонах
        liters = gallons * 3.7845; // переводим галлоны в литры

        System.out.println(gallons + " галлонов соответствуют " + liters + " литрам.");
    }
}
