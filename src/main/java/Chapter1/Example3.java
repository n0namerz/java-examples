package Chapter1;

/*
Демонстрация разницы между float и double.
 */
public class Example3 {
    public static void main(String args[]) {
        int var; // объявление целочисленной переменной
        double x; // объявление переменной с плавающей точкой

        var = 10;
        x = 10.0;

        System.out.println("Начальное значение переменной var: " + var);
        System.out.println("Начальное значение переменной var2: " + x);
        System.out.println();

        var = var / 4;
        x = x / 4;

        System.out.println("Значение var после деления: " + var);
        System.out.println("Значение x после деления: " + x);

    }
}
