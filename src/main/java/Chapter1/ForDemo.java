package Chapter1;

/*
Демонстрация использования оператора for.
 */
public class ForDemo {
    public static void main(String args[]) {
        int count; // объявляем переменную - счетчик

        for (count = 0; count < 5; count = count + 1) // count++ для инкремента, либо --count для декремента
            System.out.println("Значение переменной count: " + count);
        System.out.println("Готово!");
    }
}
