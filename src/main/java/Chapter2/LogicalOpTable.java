package Chapter2;

/*
Упражнение 2.2.
Отображение таблицы истинности для логических операций.
Добавлено изменение, которе все true = 1, false = 0.
 */
public class LogicalOpTable {
    public static void main(String args[]) {
        boolean p, q;
        System.out.println("P\tQ\tAND\tOR\tXOR\tNOT");

        p = true;
        q = true;
        String tab = "\t";
        System.out.print((p == true ? "1" : "0") + tab + (q == true ? "1" : "0") + tab);
        System.out.print(((p & q) == true ? "1" : "0") + tab + ((p | q) == true ? "1" : "0") + tab);
        System.out.println(((p ^ q) == true ? "1" : "0") + tab + ((!p) == true ? "1" : "0"));

        p = true;
        q = false;
        System.out.print((p == true ? "1" : "0") + tab + (q == true ? "1" : "0") + tab);
        System.out.print(((p & q) == true ? "1" : "0") + tab + ((p | q) == true ? "1" : "0") + tab);
        System.out.println(((p ^ q) == true ? "1" : "0") + tab + ((!p) == true ? "1" : "0"));

        p = false;
        q = true;
        System.out.print((p == true ? "1" : "0") + tab + (q == true ? "1" : "0") + tab);
        System.out.print(((p & q) == true ? "1" : "0") + tab + ((p | q) == true ? "1" : "0") + tab);
        System.out.println(((p ^ q) == true ? "1" : "0") + tab + ((!p) == true ? "1" : "0"));

        p = false;
        q = false;
        System.out.print((p == true ? "1" : "0") + tab + (q == true ? "1" : "0") + tab);
        System.out.print(((p & q) == true ? "1" : "0") + tab + ((p | q) == true ? "1" : "0") + tab);
        System.out.println(((p ^ q) == true ? "1" : "0") + tab + ((!p) == true ? "1" : "0"));
    }
}
