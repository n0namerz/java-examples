package Chapter2;

/*
Программа для конвертации дюймов в метры.
1 метр = 39.37 дюймам.
 */
public class InchToMeterTable {
    public static void main(String args[]) {
        double inches, meters;
        int counter;

        counter = 0;
        for (inches = 1; inches < 144; inches++) {
            meters = inches / 39.37; // находим количество метров в дюйме
            System.out.println(inches + " дюймов ровняется " + meters + " метрам.");
            counter++;
            if (counter == 12) {
                System.out.println();
                counter = 0;
            }
        }
    }
}
