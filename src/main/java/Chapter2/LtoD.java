package Chapter2;

/*
Демонстрация автоматического перобразования типа long в double
 */
public class LtoD {
    public static void main(String args[]) {
        long L;
        double D;

        L = 10000000L;
        D = L;

        System.out.println("L and D: " + L + " " + D);
    }
}
