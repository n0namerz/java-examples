package Chapter2;

/*
Демонстрация управляющих последовательностей в символьных строках.
 */
public class StrDemo {
    public static void main(String args[]) {
        System.out.println("Первая строка\nВторая строка"); // Перевод строки последовательностью \n
        System.out.println("A\tB\tC"); // Табуляция последовательностью \t
        System.out.println("D\tE\tF");
    }
}
