package Chapter2;

/*
В этой программе во внутренней области попытаемся создать переменную, которая объявлена во внешней области.
 */
public class NestVar {
    public static void main(String args[]) {
        int count;

        for (count = 0; count < 10; count++) {
            System.out.println("count равен: " + count);

            //int count; // НЕДОПУСТИМО! Если это раскомментировать, то программа не скомпилируется
            for (count = 0; count < 10; count++) {
                System.out.println("В этой программе ошибка, но я вам ее не покажу!");
            }
        }
    }
}
