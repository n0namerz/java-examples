package Chapter2;

/*
Демонстрация приведения типов.
 */
public class CastDemo {
    public static void main(String args[]) {
        double x, y;
        byte b;
        int i;
        char ch;

        x = 10.0;
        y = 3.0;
        i = (int) (x / y); // Приведение double к int, теряется дробная часть
        System.out.println("Целочисленное значение \"x / y\": " + i);

        i = 100;
        b = (byte) i; // Приведение int к byte
        System.out.println("Значение b: " + b);

        i = 257;
        b = (byte) i; // Приведение int к byte, но т.к. 257 byte содержать не может, то информация теряется
        System.out.println("Значение b: " + b);

        b = 88;
        ch = (char) b; // Представление символа X в коде ASCII. Явное приведение несовместимых типов.
        System.out.println("ch: " + ch);
    }
}
