package Chapter2;

/*
Упражнение 2.1
Расчитать расстояние до места вспышки молнии, звук от которого доходит до наблюдателя спустя 7.2 секунды.
 */
public class Sound {
    public static void main(String args[]) {
        int speed;
        double time, distance, distanceWithEcho;

        speed = 1100;
        time = 7.2;

        distance = speed * time;
        distanceWithEcho = speed * (time / 2); // делим время на 2, т.к. звук идеит сначала до объекта, а затем назад
        System.out.println("Расстояние до объекта: " + distance + " футов.");
        System.out.println("Расстояние до объекта, расчитанное от эха: " + distanceWithEcho + " футов.");
    }
}
