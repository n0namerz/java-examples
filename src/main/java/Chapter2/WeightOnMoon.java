package Chapter2;

/*
Программа для расчета веса на луне.
Силя тяжести на луне составляет 17% относительно земной.
 */
public class WeightOnMoon {
    public static void main(String args[]) {
        double earthWeight, moonWeight; // объявление переменных

        earthWeight = 85; // переменной веса на земле присвоено значение
        moonWeight = earthWeight * 0.17; // переменной присваивается значение земного веса умноженному на 17%

        System.out.println(earthWeight + " земных килограммов на луне будут чувствоваться как " + moonWeight);
    }
}
