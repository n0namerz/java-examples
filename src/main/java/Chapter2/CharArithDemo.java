package Chapter2;

// С символьными переменными можно обращаться как с целочисленными
public class CharArithDemo {
    public static void main(String args[]) {
        char ch;

        ch = 'X';
        System.out.println("ch содержит: " + ch);

        ch++; // инкрементируем ch
        System.out.println("Теперь ch содержит: " + ch);

        ch = 89; // хммм...
        System.out.println("Теперь ch содержит: " + ch);

        ch = 90; // типу char можно присваивать целочисленные значения
        System.out.println("Теперь ch содержит: " + ch);
    }
}
