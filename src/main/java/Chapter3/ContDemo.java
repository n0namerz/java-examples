package Chapter3;

/*
Демонстрация использования оператора continue.
 */
public class ContDemo {
    public static void main(String args[]) {
        int i;

        for (i = 0; i <= 100; i++) {
            if ((i % 2) != 0) continue; // Завершение шага итерации цикла, i не делится на 2 без остатка
            System.out.println(i);
        }
    }
}
