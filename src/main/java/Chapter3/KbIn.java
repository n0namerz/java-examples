package Chapter3;

/*
Демонстрация ввода с клавиатуры.
 */
public class KbIn {
    public static void main(String args[])
            throws java.io.IOException { // требуется для обработки ошибок, пока не рассматривается

        char ch;

        System.out.print("Нажмите нужную клавишу, а затем клавшиу Enter: ");
        ch = (char) System.in.read(); // получить символ с клавиатуры
        System.out.println("Вы нажали клавишу " + ch);
    }
}
