package Chapter3;

/*
Применение оператора break с меткой.
 */
public class Break4 {
    public static void main(String args[]) {
        int i;

        for (i = 1; i < 4; i++) {
            one:
            {
                System.out.println("Блок one выполняется.");
                two:
                {
                    System.out.println("Блок two выполняется.");
                    three:
                    {
                        System.out.println("Блок three выполняется.");
                        System.out.println("\ni равно " + i);
                        if (i == 1) break one;
                        if (i == 2) break two;
                        if (i == 3) break three;

                        // Эта строка не будет выведена, т.к. до нее никогда программа не дойдет.
                        System.out.println("не будет выведено.");

                    }
                    System.out.println("После блока three.");
                }
                System.out.println("После блока two.");
            }
            System.out.println("После блока one.");
        }
        System.out.println("После цикла for.");
    }
}

/*
Мы последовательно выходим из каждого блока, при этом каждый раз блоки выполняются.
 */