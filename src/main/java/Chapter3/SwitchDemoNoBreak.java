package Chapter3;

/*
Демонстрация использования оператора switch без использования break.
 */
public class SwitchDemoNoBreak {
    public static void main(String args[]) {
        int i;

        for (i = 0; i < 10; i++) {
            switch (i) {
                case 0:
                    System.out.println("i равно нулю.");
                case 1:
                    System.out.println("i равно единице.");
                case 2:
                    System.out.println("i равно двум.");
                case 3:
                    System.out.println("i равно трём.");
                case 4:
                    System.out.println("i равно четырём.");
                default:
                    System.out.println("i равно или больше пяти.");
            }
            System.out.println("Вышли из switch.");
            System.out.println();
        }
    }
}

/*
Можно использовать пустые case, в этом случае для всех пустых случаев будет выполняться первый не пустой.
 */