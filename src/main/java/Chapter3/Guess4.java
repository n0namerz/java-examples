package Chapter3;

/*
Игра в угадывание букв, четвертая версия.
 */
public class Guess4 {
    public static void main(String args[])
            throws java.io.IOException {
        char ch, ignore, answer = 'S';

        // первый do работает пока ch не станет равен answer
        do {
            System.out.println("Задумана буква от A до Z.");
            System.out.print("Попытайтесь её отгадать: ");

            // Получить сивол с клавиатуры
            ch = (char) System.in.read();

            // Отбросить все остальные символы во входном буфере
            do {
                ignore = (char) System.in.read();
            } while (ignore != '\n');

            if (ch == answer) System.out.println("***ПРАВИЛЬНО!***");
            else {
                System.out.print("... извините, нужная буква находится ");
                if (ch < answer) System.out.println("ближе к концу алфавита.");
                else System.out.println("ближе к началу алфавита.");
                System.out.println("Повторите попытку!\n");
            }
        } while (answer != ch);
    }
}
