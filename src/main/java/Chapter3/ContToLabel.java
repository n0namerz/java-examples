package Chapter3;

/*
Применение continue к меткам.
 */
public class ContToLabel {
    public static void main(String args[]) {
        outerLoop:
        for (int x = 0; x < 10; x++) {
            System.out.print("\nВнешний цикл: " + x + ", внутренний цикл: ");

            for (int y = 0; y < 10; y++) {
                if (y == 5) continue outerLoop;
                System.out.print(y + " ");
            }
        }

    }
}
