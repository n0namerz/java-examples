package Chapter3;

/*
Демонстрация цикла while.
 */
public class WhileDemo {
    public static void main(String args[]) {
        char ch = 'a';
        while (ch <= 'z') { // вывод букв английского алфавита
            System.out.println(ch);
            ch++;
        }
    }
}
