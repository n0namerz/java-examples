package Chapter3;

/*
Код с ошибкой. break нельзя использовать к метке, в которой этот break не определен.
Иначе говоря можно использовать тольку ту метку в цикле которой определен break.
 */
public class BreakErr {
    public static void main(String args[]) {
        int x, y;

        // Блок когда с меткой, но без break
        stop:
        for (x = 0; x < 5; x++) {
            System.out.println(x);
        }

        for (y = 0; y < 5; y++) {
            System.out.println(y);
            // if (y == 2) break stop; Это не пройдет проверку компилятором, т.к. метки stop в этом цикле нет
        }
    }
}
