package Chapter3;

/*
Пропуск отдельных составляющих в определении цикла for.
 */
public class Empty2 {
    public static void main(String args[]) {
        int i = 0;

        for (; i < 10; ) {
            System.out.println("Проход №: ");
            i++;
        }
    }
}


/*
Если при опеределения цикла for выражение вообще оставить пустым, то получится бесконечный цикл:
например for (;; )
 */