package Chapter3;

/*
Игра в угадывание букв. Вторая версия.
 */
public class Guess2 {
    public static void main(String args[])
            throws java.io.IOException {
        char ch, answer = 'X';

        System.out.println("Задумана буква от A до Z, попробуй ее отгадать.");
        System.out.println("Введи букву в верхнем регистре и нажми на ENTER: ");

        ch = (char) System.in.read();

        if (ch == answer) System.out.println("***ПРАВИЛЬНО!***");
        else System.out.println("Извини, но ты ошибся...");
    }
}

