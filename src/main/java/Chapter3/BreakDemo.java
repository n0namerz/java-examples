package Chapter3;

/*
Применение оператора break для выхода из цикла.
 */
public class BreakDemo {
    public static void main(String args[]) {
        int num = 100;
        for (int i = 0; i < num; i++) {
            if (i * i >= num) break; // Прекратить цикл, если i * i >= 100
            System.out.print(i + " ");
        }
        System.out.println("Цикл завершен.");
    }
}
