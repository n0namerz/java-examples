package Chapter3;

/*
Демонстрация многоступенчатой конструкции if-else-if.
 */
public class Ladder {
    public static void main(String args[]) {
        int x;

        for (x = 0; x < 6; x++) {
            if (x == 1) System.out.println("X равен единице.");
            else if (x == 2) System.out.println("X равен двум.");
            else if (x == 3) System.out.println("X равен трём.");
            else if (x == 4) System.out.println("X равен четырём.");
            else System.out.println("X находится вне диапазона от 1 до 4."); // условие по умолчанию
        }
    }
}
